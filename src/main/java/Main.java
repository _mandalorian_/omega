import Week1.Task3.domain.*;

public class Main {
    public static void Main(String[] args){
        Shape circle = new Circle(Color.GREEN, true,5);
        Shape rectangle = new Rectangle( Color.RED, false, 2,3);
        Shape square = new Square(4,Color.YELLOW,true );

        System.out.println(circle);
        System.out.println(rectangle);
        System.out.println(square);

        System.out.println("The Area of circle = " + ((Circle) circle).getArea());
        System.out.println("The Length of circle = " + ((Circle) circle).getPerimetr());
        System.out.println("The Area of rectangle = " + ((Rectangle) rectangle).getArea());
        System.out.println("The Perimeter of rectangle = " + ((Rectangle) rectangle).getPerimetr());
        System.out.println("The Area of square = " + ((Square) square).getArea());
        System.out.println("The Perimeter of square = " + ((Square) square).getPerimetr());

    }

}
