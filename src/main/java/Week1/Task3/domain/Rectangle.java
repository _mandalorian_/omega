package Week1.Task3.domain;

public class Rectangle extends Shape {
    private double width, lenght;

    public Rectangle(){
        this.lenght = 1.0;
        this.width = 1.0;
    }

    public Rectangle(double x, double y){
        setWidth(x);
        setLenght(y);
    }

    public Rectangle(Color x, boolean y, double q, double z){
        super(x,y);
        setWidth(q);
        setLenght(z);
    }


    public void setWidth(double x){
        this.width = x;
    }

    public void setLenght(double x){
        this.lenght = x;
    }

    public double getWidth(){
        return width;
    }

    public double getLenght(){
        return lenght;
    }

    public double getArea(){
        return width*lenght;
    }

    public double getPerimetr(){
        return 2*(width+lenght);
    }

    @Override
    public String toString() {
        return "A Rectangle with width= "+width+" and length="+lenght+", which is a subclass of"+super.toString();
    }
}
