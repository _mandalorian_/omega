package Week1.Task3.domain;

public class Square extends Rectangle{
    public Square(){
    }

    public Square(double side){
        super(side, side);
    }

    public Square(double side, Color color, boolean filled){
        super(color, filled, side, side);
    }

    public double getSide(){
        return getLenght();
    }

    public void setSide(double side){
        setLenght(side);
        setWidth(side);
    }

    public double getArea(){
        return getArea();
    }

    public double getPerimetr(){
        return getPerimetr();
    }

    @Override
    public String toString() {
        return  "A Square with side="+getWidth()+", which is a subclass of "+super.toString();
    }
}
