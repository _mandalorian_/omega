package Week1.Task3.domain;

public class Circle extends Shape{
    private double radius;

    public Circle(){
        this.radius = 1.0;
    }

    public Circle(double x){
        setRadius(x);
    }

    public  Circle(Color x, boolean y, double z){
        super(x, y);
        setRadius(z);
    }

    private void setRadius(double x) {
    this.radius = x;
    }

    public double getRadius(){
        return radius;
    }

    public double getArea(){
        return 2*3.14*Math.pow(radius,2);
    }

    public double getPerimetr(){
        return 2*3.14*radius;
    }

    @Override
    public String toString() {
        return "A Circle with radius="+radius+", which is a subclass of"+super.toString();
    }
}
