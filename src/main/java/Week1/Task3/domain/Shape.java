package Week1.Task3.domain;

public class Shape {
    private Color color;
    private boolean filled;

    public Shape(){
        this.color = Color.GREEN;
        this.filled = true;
    }

    public Shape(Color x, boolean y) {
        setColor(x);
        setFilled(y);
    }



    public void setFilled(boolean x){
        this.filled = x;
    }

    public boolean isFilled(){
        return filled;
    }

    public void setColor(Color x){
        this.color = x;
    }

    public Color getColor(){
        return color;
    }

    @Override
    public String toString() {
        return "A Shape with color of"+color+" and "+ filled;
    }

}
